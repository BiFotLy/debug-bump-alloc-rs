use bump_allocator::{DebugBumpAllocator, Locked};

const HEAP_SIZE: usize = 1024 * 1024; // 1 MiB

#[global_allocator]
static ALLOCATOR: Locked<DebugBumpAllocator> = Locked::new(DebugBumpAllocator::new(HEAP_SIZE));

fn main() {
    #[cfg(not(test))]
    println!(
        "panic_alloc_in_runtime\n\
         panic_dealloc_in_runtime\n\
         panic_overflow_heap"
    );
}

#[cfg(test)]
mod tests {
    use super::*;
    use bump_allocator::AllocMode;
    use std::mem::drop;

    #[test]
    fn pass_alloc_large_vec() {
        print!("Starting test: 'alloc_large_arr'... ");
        let _ = vec![1u8; HEAP_SIZE / 100 * 90];
        println!("PASSED");
    }

    #[allow(dead_code)]
    struct StructWithAlignedVals {
        smol: u8,
        big: u32,
        biggest: u128,
        large_arr_of_smols: [u16; 10000],
    }

    #[test]
    fn pass_alloc_dealloc_in_setup() {
        print!("Starting test: 'alloc_dealloc_in_setup'... ");
        let test_var = Box::new(1);
        let test_struct = StructWithAlignedVals {
            smol: 1,
            big: 1000,
            biggest: 10000000,
            large_arr_of_smols: [300; 10000],
        };
        let test_str = "Test string".to_string();
        drop(test_var);
        drop(test_struct);
        drop(test_str);
        println!("PASSED");
    }

    #[test]
    #[allow(unused_variables)]
    fn panic_alloc_in_runtime() {
        println!(
            "Starting test: 'alloc_in_runtime'...\n\
         It should panic with 'Alloc in runtime' error"
        );
        ALLOCATOR.set_mode(AllocMode::Runtime);
        let arr = Box::new(1);
        ALLOCATOR.set_mode(AllocMode::Setup);
    }

    #[test]
    #[allow(unused_variables)]
    fn panic_dealloc_in_runtime() {
        println!(
            "Starting test: 'dealloc_in_runtime'...\n\
         It should panic with 'Dealloc in runtime' error"
        );
        let arr = Box::new(1);
        ALLOCATOR.set_mode(AllocMode::Runtime);
        drop(arr);
        ALLOCATOR.set_mode(AllocMode::Setup);
    }

    #[test]
    fn pass_switch_between_modes() {
        print!("Starting test: 'switch_between_modes'... ");
        ALLOCATOR.set_mode(AllocMode::Setup);
        ALLOCATOR.set_mode(AllocMode::Setup);
        ALLOCATOR.set_mode(AllocMode::Runtime);
        ALLOCATOR.set_mode(AllocMode::Runtime);
        ALLOCATOR.set_mode(AllocMode::Setup);
        ALLOCATOR.set_mode(AllocMode::Runtime);
        ALLOCATOR.set_mode(AllocMode::Setup);
        println!("PASSED");
    }

    #[test]
    fn panic_overflow_heap() {
        println!(
            "Starting test: 'overflow_heap'...\n\
         It should panic with 'Not enough memory' error"
        );
        let _ = Box::new([1u8; HEAP_SIZE + 1]);
    }
}
