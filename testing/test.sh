#!/bin/sh

set -e

alias print_panic_tests='cargo run -p bump_allocator_testing'
alias run_test='cargo test -p bump_allocator_testing'

tests_without_panic() {
    run_test pass_
}

tests_with_panic() {
    for TEST_NAME in $(print_panic_tests)
    do
        run_test $TEST_NAME && exit 1 || true
    done
}

tests_without_panic
tests_with_panic

echo -e "\n<<< ALL TESTS ARE PASSED >>>\n"