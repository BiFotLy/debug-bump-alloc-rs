[[Русский](./docs/README-ru.md)]

---

# Debug Bump Allocator

This is a `Bump Allocator`, which does several things:

- allocates a given heap size;
- on each allocation request, returns a pointer to a memory address;
- prints info about allocation;
- panics depending on program mode (setup or runtime).

## Allocator

Bump allocator is a simple allocator, which allocates memory linearly by moving pointer from the start to the end of the preallocated heap.

On 0 allocations, `free ptr` is equal to the start memory address of the heap. Each time an allocation occurs, `free ptr` is increased by the allocation size and, if necessary, by the memory align size.

Memory never frees, because `free ptr` can move only in a single direction. At the end of the heap there is no free memory left, and so program exits with an `out of memory` error on the next allocation.

The bump allocator implementation in this crate doesn't free memory, even if the heap has 0 pointers to its addresses.

![Bump Allocator Scheme](./docs/bump_allocator_scheme.png)

## Usage

Initialize:

```
use bump_allocator::{AllocMode, DebugBumpAllocator, Locked};

const HEAP_SIZE: usize = 1024 * 1024; // 1 MiB

#[global_allocator]
static ALLOCATOR: Locked<DebugBumpAllocator> = Locked::new(DebugBumpAllocator::new(HEAP_SIZE));
```

Upon initialization allocator starts working in `setup` mode.

## Program modes

In `setup` mode allocations and deallocations are _permited_. Switching to `setup` mode:

```
ALLOCATOR.set_mode(AllocMode::Setup);
```

In `runtime` mode they are _prohibited_, so when either occurs, the program panics. Switching to `runtime` mode:

```
ALLOCATOR.set_mode(AllocMode::Runtime);
```

Upon entering `runtime` mode, summary about allocations is being printed to `stderr`.

```
< Memory footprint >
Heap size:   134217728 bytes
Allocations: 3
Allocated:   1000053 bytes
Free:        133217675 bytes
```

## Info about allocations

Example of printed information:

```
A 1 140375652691984 +5    bytes
A 2 140375652691992 +48   bytes
A 3 140375652692040 +4    bytes
A 4 140375652692044 +1048576  bytes
A 5 140375653740620 +4    bytes
A 6 140375653740624 +9    bytes
```

Information is printed to `stderr` and has the following structure:

| type | number | memory pointer address in decimal | size |       |
| :--: | :----: | :-------------------------------: | :--: | ----- |
|  A   |   7    |          140375653740633          |  +1  | bytes |
|  A   |   8    |          140375653740640          | +16  | bytes |
|  D   |        |          140375653740640          | +16  | bytes |

Type can be either `A` (Allocation) or `D` (Deallocation).

To disable information printing, disable default features for this package in `Cargo.toml`:

```
bump_allocator = { git = "https://gitlab.com/BiFotLy/debug-bump-alloc-rs", default-features = false }
```

## Testing

Run all tests:

```
% ./testing/test.sh
...
...
<<< ALL TESTS ARE PASSED >>>
```

## Links

- [Rust Doc](https://bifotly.gitlab.io/debug-bump-alloc-rs)
