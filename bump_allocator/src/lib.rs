//! Simple `Bump Allocator` with information about allocations.
//!
//! It does several things:
//! - allocates a given heap size;
//! - on each allocation request, returns a pointer to a memory address;
//! - prints info about allocation;
//! - panics depending on program mode (setup or runtime).

mod allocator;
pub use allocator::{AllocMode, DebugBumpAllocator};

mod utils;
pub use utils::Locked;

#[cfg(feature = "info")]
mod info;

#[cfg(feature = "info")]
use info::{print_mem_info, AllocType};
