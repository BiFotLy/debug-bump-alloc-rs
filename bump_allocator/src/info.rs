use std::cmp::max;

pub enum AllocType {
    Allocation,
    Deallocation,
}

/// Get digits count in a number.
fn get_n_digits(num: usize) -> usize {
    max(((num as f64).log10()) as usize + 1, 1)
}

/// Print information about allocation.
///
/// Info structure: ```type | number | memory ptr in decimal | size```
///
/// Info example: ```A 3 0x140516917620808 +48   bytes```
pub fn print_mem_info(address: usize, size: usize, alloc_type: AllocType, allocations: usize) {
    match alloc_type {
        AllocType::Allocation => {
            eprint!("A {} ", allocations);
        }
        AllocType::Deallocation => {
            let allocs_width = get_n_digits(allocations);
            eprint!("D {:width$} ", ' ', width = allocs_width);
        }
    }

    let size_width = 4 * ((get_n_digits(size) - 1) / 4 + 1);

    eprintln!(
        "{} +{:<size_width$} bytes",
        address,
        size,
        size_width = size_width
    );
}
