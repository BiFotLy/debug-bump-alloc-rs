use crate::Locked;
use core::panic;
use std::alloc::{GlobalAlloc, Layout};

#[cfg(feature = "info")]
use crate::{print_mem_info, AllocType};

/// Enum with allocation modes.
pub enum AllocMode {
    /// Allocations and deallocations are _permited_.
    Setup,
    /// Allocations and deallocations are _prohibited_.
    Runtime,
}

/// Struct which holds `Bump Allocator`.
pub struct DebugBumpAllocator {
    /// For tracking if the heap is allocated or not.
    acquired: bool,
    next_free_address: usize,
    heap_size: usize,
    remaining: usize,
    #[cfg(feature = "info")]
    allocations: usize,
    is_setup_mode: bool,
}

impl DebugBumpAllocator {
    /// Create a new empty bump allocator with given heap_size.
    ///
    /// Const is neeeded because `#[global_acllocator]` applies only to a static value.
    ///
    /// * `heap_size` - Heap size in bytes.
    pub const fn new(heap_size: usize) -> Self {
        Self {
            acquired: false,
            next_free_address: 0,
            heap_size,
            remaining: heap_size,
            #[cfg(feature = "info")]
            allocations: 0,
            is_setup_mode: true,
        }
    }
}

impl Locked<DebugBumpAllocator> {
    /// Switch between `setup` and `runtime` modes.
    pub fn set_mode(&self, alloc_mode: AllocMode) {
        let mut allocator = self.lock();

        match alloc_mode {
            AllocMode::Setup => {
                #[cfg(feature = "info")]
                eprintln!("Entering setup mode.");
                allocator.is_setup_mode = true;
            }
            AllocMode::Runtime => {
                #[cfg(feature = "info")]
                {
                    eprintln!("Entering runtime mode.");
                    self.print_total_alloc_size();
                }
                allocator.is_setup_mode = false;
            }
        }
    }

    #[cfg(feature = "info")]
    fn print_total_alloc_size(&self) {
        let allocator = self.lock();

        let heap_size = allocator.heap_size;
        let free = allocator.remaining;
        let allocations = allocator.allocations;
        let allocated = heap_size - free;
        eprintln!(
            "< Memory footprint >\n\
             Heap size:   {heap_size} bytes\n\
             Allocations: {allocations}\n\
             Allocated:   {allocated} bytes\n\
             Free:        {free} bytes"
        )
    }
}

/// Align the given address `addr` upwards to alignment (a multiple of) `align`.
///
/// `align` needs to be a power of two.
fn addr_round_up(addr: usize, align: usize) -> usize {
    (addr + align - 1) & !(align - 1)
}

unsafe impl GlobalAlloc for Locked<DebugBumpAllocator> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let mut allocator = self.lock();

        // Checks if a heap with given size was allocated or not.
        // If not, then allocates it.
        if !allocator.acquired {
            allocator.next_free_address = libc::malloc(allocator.heap_size) as usize;
            allocator.acquired = true;
        }

        let size = layout.size();
        if size > allocator.remaining {
            drop(allocator);
            panic!("Not enough memory in the heap.");
        } else {
            allocator.remaining -= size;
        }

        let address = addr_round_up(allocator.next_free_address, layout.align());
        allocator.next_free_address = address + size;

        #[cfg(feature = "info")]
        {
            allocator.allocations += 1;
            print_mem_info(
                address,
                layout.size(),
                AllocType::Allocation,
                allocator.allocations,
            );
        }

        if allocator.is_setup_mode {
            address as _
        } else {
            drop(allocator);
            panic!("Allocation occured in runtime mode.");
        }
    }

    #[cfg_attr(not(feature = "info"), allow(unused_variables))]
    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        let allocator = self.lock();

        #[cfg(feature = "info")]
        {
            print_mem_info(
                ptr as usize,
                layout.size(),
                AllocType::Deallocation,
                allocator.allocations,
            );
        }

        if !allocator.is_setup_mode {
            drop(allocator);
            panic!("Deallocation occured in runtime mode.");
        }
    }
}
